/* * libeisnoise - Adds realistic noise to EIS spectra
 * Copyright (C) 2025 Carl Klemm <carl@uvos.xyz>
 *
 * This file is part of libeisnoise.
 *
 * libeisnoise is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libeisnoise is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libeisnoise.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include <stdint.h>

uint64_t murmurHash64(const void* key,int len, uint64_t seed);
