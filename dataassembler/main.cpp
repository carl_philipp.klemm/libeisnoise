//
// libeisnoise - Adds realistic noise to EIS spectra
// Copyright (C) 2025 Carl Klemm <carl@uvos.xyz>
//
// This file is part of libeisnoise.
//
// libeisnoise is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// libeisnoise is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with libeisnoise.  If not, see <http://www.gnu.org/licenses/>.
//

#include <filesystem>
#include <eisgenerator/model.h>
#include <eisgenerator/basicmath.h>
#include <functional>
#include <algorithm>
#include <execution>
#include <mutex>
#include <sstream>
#include <kisstype/spectra.h>

#include "microtar.h"

bool isCsv(const std::filesystem::path& path)
{
	return path.extension() == ".csv";
}

std::vector<std::filesystem::path> getFilesInDirectories(const std::filesystem::path& path, std::function<bool(const std::filesystem::path&)> match)
{
	std::vector<std::filesystem::path> out;
	for(const std::filesystem::directory_entry& dirent : std::filesystem::directory_iterator{path})
	{
		if(std::filesystem::is_directory(dirent.path()))
		{
			std::vector<std::filesystem::path> subdir = getFilesInDirectories(dirent.path(), match);
			out.insert(out.end(), subdir.begin(), subdir.end());
		}
		else if(std::filesystem::is_regular_file(dirent.path()))
		{
			if(match(dirent.path()))
				out.push_back(dirent.path());
		}
	}
	return out;
}

class consistency_error: public std::exception
{
	std::string whatStr;
public:
	consistency_error(const std::string& whatIn): whatStr(whatIn)
	{}
	virtual const char* what() const noexcept override
	{
		return whatStr.c_str();
	}
};

eis::Spectra loadAndDiffFile(const std::filesystem::path& path)
{
	eis::Spectra spectra = eis::Spectra::loadFromDisk(path);
	spectra.header = "";

	std::sort(spectra.data.begin(), spectra.data.end());

	eis::Model model(spectra.model);

	std::vector<fvalue> omega(spectra.data.size());
	for(size_t i = 0; i < spectra.data.size(); ++i)
		omega[i] = spectra.data[i].omega;

	std::vector<eis::DataPoint> genData = model.executeSweep(omega);
	fvalue dist = eisNyquistDistance(spectra.data, genData);
	if(std::isnan(dist))
		throw consistency_error("spectra dosent appear to be fitted correctly dist is NAN!\n");
	else if(std::isinf(dist) || dist > 2.0)
		throw consistency_error("spectra dosent appear to be fitted correctly, dist is too large!\n");

	std::vector<eis::DataPoint> raw = spectra.data;
	eis::difference(spectra.data, genData);

	for(size_t i = 0; i < raw.size(); ++i)
	{
		if(std::abs(raw[i].im) > 0.001)
			spectra.data[i].im = spectra.data[i].im / raw[i].im;
		else
			spectra.data[i].im = spectra.data[i].im / static_cast<fvalue>(0.001);
	}

	return spectra;
}

int main(int argc, char** argv)
{
	if(argc < 3)
	{
		if(argc)
			std::cout<<"Useage "<<argv[0]<<": [DATASET_DIR] [OUT_DIFF_DATASET]\n";
		return 1;
	}

	std::vector<std::filesystem::path> files = getFilesInDirectories(argv[1], &isCsv);
	std::vector<eis::Spectra> spectras;
	spectras.reserve(files.size());
	std::mutex spectrasMutex;

	std::for_each(std::execution::par, files.begin(), files.end(), [&spectras, &spectrasMutex](const std::filesystem::path path)
	{
		try
		{
			eis::Spectra spectra = loadAndDiffFile(path);
			spectrasMutex.lock();
			std::cout<<"Loaded "<<path<<'\n';
			spectras.push_back(spectra);
			spectrasMutex.unlock();
		}
		catch(const eis::file_error& err)
		{
			spectrasMutex.lock();
			std::cout<<"Could not load "<<path<<"; "<<err.what()<<'\n';
			spectrasMutex.unlock();
		}
		catch(const eis::parse_errror& err)
		{
			spectrasMutex.lock();
			std::cout<<"Could not parse the model from "<<path<<"; "<<err.what()<<'\n';
			spectrasMutex.unlock();
		}
		catch(const consistency_error& err)
		{
			spectrasMutex.lock();
			std::cout<<"Model and data are inconsitant for "<<path<<"; "<<err.what()<<'\n';
			spectrasMutex.unlock();
		}
	});

	mtar_t tar;
	int ret = mtar_open(&tar, argv[2], "w");
	if(ret < 0)
	{
		std::cout<<"coult not open "<<argv[2]<<' '<<mtar_strerror(ret)<<'\n';
		return 2;
	}

	for(size_t i = 0; i < spectras.size(); ++i)
	{
		std::stringstream ss;
		spectras[i].saveToStream(ss);

		ret = mtar_write_file_header(&tar, (std::to_string(i) + ".csv").c_str(), ss.str().size());
		if(ret < 0)
		{
			std::cout<<"coult not write header "<<mtar_strerror(ret)<<'\n';
			continue;
		}
		ret = mtar_write_data(&tar, ss.str().c_str(), ss.str().size());
		if(ret < 0)
			std::cout<<"coult not write data "<<mtar_strerror(ret)<<'\n';
	}

	mtar_finalize(&tar);
	mtar_close(&tar);

	return 0;
}
