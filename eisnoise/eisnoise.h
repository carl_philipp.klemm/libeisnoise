/* * libeisnoise - Adds realistic noise to EIS spectra
 * Copyright (C) 2025 Carl Klemm <carl@uvos.xyz>
 *
 * This file is part of libeisnoise.
 *
 * libeisnoise is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libeisnoise is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libeisnoise.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <kisstype/type.h>
#include <kisstype/spectra.h>
#include <vector>
#include <exception>
#include <string>

class EisNoise
{
private:

	struct Noise
	{
		std::string name;
		fvalue omegaMax;
		fvalue omegaMin;
		eis::Spectra spectra;
	};
	std::vector<Noise> noises;

	static void addImpl(std::vector<eis::DataPoint>& spectra, const Noise& noise, fvalue intensity, bool ignFreq);

	fvalue calcMaxIntensity(std::vector<eis::DataPoint>& spectra);

public:
	class apply_err: public std::exception
	{
		std::string whatStr;
	public:
		apply_err(const std::string& whatIn): whatStr(whatIn)
		{}
		virtual const char* what() const noexcept override
		{
			return whatStr.c_str();
		}
	};

	EisNoise(fvalue maxIntensity = -1);
	~EisNoise() = default;

	void add(std::vector<eis::DataPoint>& spectra, fvalue intensity = 1.0, bool ignFreq = false);
	void addSpecific(std::vector<eis::DataPoint>& spectra, const std::string& name , fvalue intensity = 1.0,  bool ignFreq = false);
	std::vector<std::string> getAvailable();
};
