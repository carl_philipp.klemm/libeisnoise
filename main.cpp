//
// libeisnoise - Adds realistic noise to EIS spectra
// Copyright (C) 2025 Carl Klemm <carl@uvos.xyz>
//
// This file is part of libeisnoise.
//
// libeisnoise is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// libeisnoise is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with libeisnoise.  If not, see <http://www.gnu.org/licenses/>.
//

#include "eisnoise/eisnoise.h"
#include "log.h"
#include <eisgenerator/model.h>
#include <iostream>
#include <valarray>
#include <vector>
#include <sciplot/sciplot.hpp>
#include <random>
#include <eisgenerator/basicmath.h>
#include <eisgenerator/normalize.h>


int main(int argc, char** argv)
{
	if(argc < 2)
	{
		if(argc)
			std::cerr<<"Usage: "<<argv[0]<<" [MODEL_STR]\n";
		return 1;
	}

	eis::Model model(argv[1], 20, true);
	eis::Range omega(1, 1e6, 50, true);
	eis::Log::level = eis::Log::DEBUG;

	std::vector<size_t> recommendedIndecies = model.getRecommendedParamIndices(omega, 0.01, true);

	EisNoise noise(0.2);
	std::random_device rd;
	std::mt19937_64 gen(rd());
	std::uniform_int_distribution<size_t> distrib(0, recommendedIndecies.size()-1);

	/*std::vector<std::string> names = noise.getAvailable();
	for(const std::string& name : names)
		std::cout<<name<<'\n';*/

	sciplot::Plot2D plot;
	plot.xlabel("Re");
	plot.ylabel("Im");

	double xmin = std::numeric_limits<double>::max();
	double xmax = std::numeric_limits<double>::min();
	double ymin = std::numeric_limits<double>::max();
	double ymax = std::numeric_limits<double>::min();

	for(size_t i = 0; i < 100; ++i)
	{
		size_t index = distrib(gen);
		std::vector<eis::DataPoint> data = model.executeSweep(omega, recommendedIndecies[index]);
		noise.add(data);
		eis::normalize(data);

		std::pair<std::valarray<fvalue>, std::valarray<fvalue>> arrays = eis::eisToValarrays(data);

		double cxmin = arrays.first.min();
		cxmin = cxmin - std::abs(cxmin*0.05);
		double cymin = arrays.second.min();
		cymin = cymin - std::abs(cymin*0.05);

		double cxmax = arrays.first.max();
		cxmax = cxmax - std::abs(cxmax*0.05);
		double cymax = arrays.second.max();
		cymax = cymax - std::abs(cymax*0.05);

		if(xmax < cxmax)
			xmax = cxmax;
		if(ymax < cymax)
			ymax = cymax;

		if(xmin > cxmin)
			xmin = cxmin;
		if(ymin > cymin)
			ymin = cymin;
		plot.xrange(xmin, xmax);
		plot.yrange(ymin, ymax);

		plot.drawCurve(arrays.first, arrays.second);

		eis::Spectra(data, model.getModelStrWithParam(index), "").saveToStream(std::cout);
		std::cout<<eis::nonConstantScore(data)<<'\n';

	}

	plot.legend().hide();
	sciplot::Figure fig({{plot}});
	sciplot::Canvas canvas({{fig}});
	canvas.size(1280, 720);
	canvas.show();

	return 0;
}
