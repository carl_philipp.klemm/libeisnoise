//
// libeisnoise - Adds realistic noise to EIS spectra
// Copyright (C) 2025 Carl Klemm <carl@uvos.xyz>
//
// This file is part of libeisnoise.
//
// libeisnoise is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// libeisnoise is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with libeisnoise.  If not, see <http://www.gnu.org/licenses/>.
//

#include "eisnoise/eisnoise.h"

#include <cmath>
#include <limits>
#include <numeric>
#include <sstream>
#include <random>
#include <iostream>
#include <list>
#define INCBIN_PREFIX r
#include "incbin.h"
#include "microtar.h"
#include <kisstype/type.h>
#include <eisgenerator/basicmath.h>

INCBIN(Tar, DATA_DIR "/data.tar");

void EisNoise::addImpl(std::vector<eis::DataPoint>& spectra, const Noise& noise, fvalue intensity, bool ignFreq)
{
	if(!ignFreq && (spectra.front().omega < noise.omegaMin || spectra.back().omega > noise.omegaMax))
		throw apply_err("Datapoints of spectra are out of range for the available noise");

	std::vector<fvalue> omegas(spectra.size());
	for(size_t i = 0; i < omegas.size(); ++i)
		omegas[i] = spectra[i].omega;

	size_t noiseIndex = 0;
	for(size_t i = 0; i < spectra.size(); ++i)
	{
		while(noiseIndex < noise.spectra.data.size() && spectra[i].omega > noise.spectra.data[noiseIndex].omega)
			++noiseIndex;

		std::complex<fvalue> noiseValue;
		if(noiseIndex > 0 &&
			std::abs(noise.spectra.data[noiseIndex].omega - spectra[i].omega) > std::abs(noise.spectra.data[noiseIndex-1].omega - spectra[i].omega))
		{
			noiseValue = noise.spectra.data[noiseIndex-1].im;
		}
		else
		{
			noiseValue = noise.spectra.data[noiseIndex].im;
		}

		if(std::abs(spectra[i].im) > 0.001)
		{
			spectra[i].im = (noiseValue * spectra[i].im) * intensity + spectra[i].im;
		}
		else
		{
			std::complex<fvalue> smallVal(std::copysign(0.001, spectra[i].im.real()), std::copysign(0.001, spectra[i].im.imag()));
			spectra[i].im = (noiseValue * smallVal) * intensity + spectra[i].im;
		}
	}
}

void EisNoise::add(std::vector<eis::DataPoint>& spectra, fvalue intensity, bool ignFreq)
{
	std::sort(spectra.begin(), spectra.end());
	std::vector<size_t> indecies(spectra.size());
	std::iota(indecies.begin(), indecies.end(), 0);
	std::random_device rd;
	std::mt19937_64 gen(rd());

	while(!indecies.empty())
	{
		std::uniform_int_distribution<size_t> distrib(0, indecies.size()-1);
		volatile size_t indexindex = distrib(gen);
		size_t index = indecies[indexindex];
		if(!ignFreq && (spectra.front().omega < noises[index].omegaMin || spectra.back().omega > noises[index].omegaMax))
		{
			indecies.erase(indecies.begin() + index);
			continue;
		}

		addImpl(spectra, noises[index], intensity, ignFreq);
		return;
	}

	throw apply_err("Datapoints of spectra are out of range for the available noise");
}

void EisNoise::addSpecific(std::vector<eis::DataPoint>& spectra, const std::string& name, fvalue intensity, bool ignFreq)
{
	std::sort(spectra.begin(), spectra.end());
	for(const Noise& noise :noises)
	{
		if(noise.name == name)
		{
			addImpl(spectra, noise, intensity, ignFreq);
			return;
		}
	}

	throw apply_err(name + " dose not exist");
}

std::vector<std::string> EisNoise::getAvailable()
{
	std::vector<std::string> out;
	out.reserve(noises.size());

	for(const Noise& noise :noises)
		out.push_back(noise.name);

	return out;
}

fvalue EisNoise::calcMaxIntensity(std::vector<eis::DataPoint>& spectra)
{
	fvalue reMax = std::numeric_limits<fvalue>::min();
	fvalue imMax = std::numeric_limits<fvalue>::min();

	for(const eis::DataPoint& point : spectra)
	{
		fvalue re = std::abs(point.im.real());
		fvalue im = std::abs(point.im.imag());
		if(im > imMax)
			imMax = im;
		if(re > reMax)
			reMax = re;
	}

	return std::max(reMax, imMax);
}

EisNoise::EisNoise(fvalue maxIntensity)
{
	mtar_t tar;

	int ret = mtar_open_mem(&tar, (void*)rTarData, rTarSize, "r");
	assert(ret == MTAR_ESUCCESS);

	mtar_header_t h;
	while(mtar_read_header(&tar, &h) != MTAR_ENULLRECORD)
	{
		char *buff = new char[h.size];
		mtar_read_data(&tar, buff, h.size);
		std::istringstream ss(std::string(buff, h.size));
		Noise noise;
		noise.spectra = eis::Spectra::loadFromStream(ss);
		noise.name = h.name;
		noise.omegaMax = noise.spectra.data.back().omega;
		noise.omegaMin = noise.spectra.data.front().omega;
		assert(noise.omegaMax > noise.omegaMin);


		if(maxIntensity > 0)
		{
			fvalue maxIntensityForSpectra = calcMaxIntensity(noise.spectra.data);
			if(maxIntensityForSpectra > maxIntensity)
				eis::mulitplyAdd(noise.spectra.data, maxIntensity/maxIntensityForSpectra, 0);
		}

		noises.push_back(noise);
		mtar_next(&tar);
	}

	mtar_close(&tar);
}
